<?php

echo additionRomaine('VII', 'XI');

function additionRomaine(string $val1, string $val2):int {
    $calcul = convertRomanNumber($val1) + convertRomanNumber($val2);
    
    echo $calcul;
    echo '<br>';
    return convertNormalNumber($calcul);
}

function convertRomanNumber(string $roman): int{
    $roman_number_list = getRomanArray();
    $normal_number = 0;
    foreach ($roman_number_list as $key => $value){
        while(strpos($roman, $key) === 0){
            $normal_number = $normal_number + $value;
            $roman = substr($roman, strlen($key));
        }
    }
    
    return $normal_number;
}

function convertNormalNumber(int $number):string{
    $roman_number = "";
    $roman_number_list = getRomanArray();
   
    foreach ($roman_number_list as $key=>$value){
        $found = intval($number/$value);
        
        $roman_number .= str_repeat($key, $found);
        
        $number = $number % $value;
    }
    return $roman_number;
}


function getRomanArray(){
    $roman_number_list = array(
    'M' => 1000,
    'CM'=> 900,
    'D' => 500,
    'CD' => 400,
    'C' => 100,
    'XC' => 90,
    'L' => 50,
    'XL' => 40,
    'X' => 10,
    'IX' => 9,
    'V' => 5,
    'IV' => 4,
    'I' => 1
    );
    return $roman_number_list;
}